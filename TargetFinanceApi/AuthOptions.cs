﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace TargetFinanceApi
{
    public class AuthOptions
    {
        /// <summary>
        /// Издатель токена
        /// </summary>
        public const string ISSUER = "TargetFinanceApi_SERVER"; // издатель токена

        /// <summary>
        /// Потребитель токена
        /// </summary>
        public const string AUDIENCE = "TargetFinanceApi_CLIENT"; // потребитель токена

        /// <summary>
        /// Ключ, который будет применяться для создания токена
        /// </summary>
        const string KEY = "QAS#@$GH&^&%G$#^";

        /// <summary>
        /// Время жизни токена (60 минут)
        /// </summary>
        public const int LIFETIME = 60;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
