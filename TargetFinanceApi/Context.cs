﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;
using TargetFinanceApi.Models;

namespace TargetFinanceApi
{
    /// <summary>
    /// Сеанс с базой данных, использоваться для запроса и сохранения экземпляров сущностей.
    /// </summary>
    public class Context : DbContext
    {
        public DbSet<RoleEntity> Roles { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<PortfolioEntitiy> Portfolios { get; set; }
        public DbSet<AssetCategoryEntity> AssetCategories { get; set; }
        public DbSet<SymbolEntity> Symbols { get; set; }
        public DbSet<ClassActiveEntity> ClassActives { get; set; }
        public DbSet<StatusEntity> Statuses { get; set; }
        public DbSet<CurrencyEntity> Currencies { get; set; }
        public DbSet<PortfolioTypeEntitiy> PortfolioTypes { get; set; }
        public DbSet<TradeEntity> Trades { get; set; }
        public DbSet<ImportEntity> LogsImport { get; set; }
        public DbSet<LinkImportAndTradeEntity> LogsImportLink { get; set; }

        /// <summary>
        /// Настройка БД
        /// </summary>
        /// <param name="optionsBuilder">Построитель, используемый для создания или изменения параметров этого контекста</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // string connectionString = @"Data Source=192.168.50.100;Initial Catalog=target-finance;Persist Security Info=True;User ID=admin;Password=C0ntr01_Admin";
            string connectionString = @"Data Source=192.168.50.100;Initial Catalog=target-finance;Persist Security Info=True;User ID=sa;Password=C0ntr01_Admin";
            optionsBuilder.UseSqlServer(connectionString);
            optionsBuilder.UseLazyLoadingProxies().UseSqlServer(connectionString);
            base.OnConfiguring(optionsBuilder);
        }

        /// <summary>
        /// Создание и настройка модели
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RoleEntity>().HasData(
                new() { Name = "Администратор", RoleId = 1 },
                new() { Name = "Пользователь", RoleId = 2 }
            );

            modelBuilder.Entity<CurrencyEntity>().HasData(
                new CurrencyEntity() { CurrencyId = 1, Name = "Российский рубль", Symbol = "RUB" },
                new CurrencyEntity() { CurrencyId = 2, Name = "ЕВРО", Symbol = "EUR" },
                new CurrencyEntity() { CurrencyId = 3, Name = "Доллар США", Symbol = "USD" }
            );

            modelBuilder.Entity<PortfolioTypeEntitiy>().HasData(
                new PortfolioTypeEntitiy() { PortfolioTypeId = 1, Name = "Стандарт" }
            );



            base.OnModelCreating(modelBuilder);

            // Установка значений по умолчанию
            modelBuilder.Entity<UserEntity>().Property(u => u.DtRegistration).HasDefaultValueSql("getdate()");
            modelBuilder.Entity<PortfolioEntitiy>().Property(p => p.DtOpening).HasDefaultValueSql("getdate()");
        }
    }
}
