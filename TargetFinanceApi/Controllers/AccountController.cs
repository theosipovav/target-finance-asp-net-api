﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;
using TargetFinanceApi.Models;

namespace TargetFinanceApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        /// <summary>
        /// Jбъект учетной записи пользователя
        /// </summary>
        private new UserEntity User { get; set; }

        /// <summary>
        /// Контекст подключения к базе данных;
        /// </summary>
        private Context DbContext { get; }

        /// <summary>
        /// Контроллер авторизации
        /// </summary>
        public AccountController(Context context)
        {
            User = null;
            DbContext = context;
        }

        /// <summary>
        /// Авторизация пользователя в системе
        /// </summary>
        /// <param name="form">Данные формы</param>
        [HttpPost]
        public ActionResult<UserEntity> Signin([FromBody] SigninForm form)
        {
            try
            {
                string login = form.Login;
                string password = form.Password;
                ClaimsIdentity identity = GetIdentity(login, password);
                if (identity == null) throw new Exception("Пользователь не найден или введен неверный логин или пароль");
                DateTime now = DateTime.UtcNow;
                JwtSecurityToken jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));
                string encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                int expirationTime = AuthOptions.LIFETIME * 1 * 1000 * 60;
                return Ok(new
                {
                    user = User,
                    token = encodedJwt,
                    expirationTime = expirationTime
                });

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Аутентификация пользователя
        /// </summary>
        /// <param name="login">Логин</param>
        /// <param name="password">Пароль</param>
        private ClaimsIdentity GetIdentity(string login, string password)
        {
            User = this.DbContext.Users.FirstOrDefault(x => x.Login == login && x.Password == password);
            if (User != null)
            {
                List<Claim> claims = new()
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, User.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, User.Role.Name)
                };
                ClaimsIdentity claimsIdentity = new(claims, "Token", ClaimsIdentity.DefaultNameClaimType,ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            // если пользователя не найдено
            return null;
        }

        /// <summary>
        /// Регистрация нового пользователя в системе
        /// </summary>
        /// <param name="form">Данные формы</param>
        [HttpPost]
        public ActionResult<UserEntity> Signup([FromBody] SignupForm form)
        {
            try
            {
                if (form.Password != form.PasswordRepeat) throw new Exception("Пароли не совпадают");
                UserEntity user = DbContext.Users.FirstOrDefault(x => x.Login == form.Login || x.Email == form.Email);
                if (user != null) throw new Exception("Пользователь с таким же логином или email уже существует!");
                RoleEntity role = null;
                if (!DbContext.Users.Any()) role = DbContext.Roles.Find(1);
                else role = DbContext.Roles.Find(2);
                user = new()
                {
                    Login = form.Login,
                    Password = form.Password,
                    Role = role,
                    Email = form.Email,
                    DtRegistration = DateTime.Now,
                    Name = form.Name,
                };
                DbContext.Users.Add(user);
                DbContext.SaveChanges();
                return Ok(new { user });
            }
            catch (Exception ex)
            {
                return BadRequest(new { errorText = ex.Message });
            }
        }

        /// <summary>
        /// Получить коллекцию всех пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserEntity>>> Users()
        {
            try
            {
                return await DbContext.Users.ToListAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
