﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Controllers
{
    /// <summary>
    /// Управление классами актива
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    [EnableCors("_myAllowSpecificOrigins")]
    public class AssetCategoryController : ControllerBase
    {
        /// <summary>
        /// Контекст работы с базой данных
        /// </summary>
        private readonly Context DbContext;

        public AssetCategoryController(Context context)
        {
            DbContext = context;
        }


        /// <summary>
        /// Получить записи
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AssetCategoryEntity>>> GetAll()
        {
            return await DbContext.AssetCategories.ToListAsync();
        }


        /// <summary>
        /// Создать новую запись
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<AssetCategoryEntity>> Create([FromForm] IFormCollection form)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();

                AssetCategoryEntity assetCategory = new()
                {
                    Name = form["name"]
                };
                DbContext.AssetCategories.Add(assetCategory);
                await DbContext.SaveChangesAsync();
                return assetCategory;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Обновить данные записи
        /// </summary>
        [HttpPut]
        public async Task<ActionResult<AssetCategoryEntity>> Update([FromForm] IFormCollection form)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();
                if (Int32.TryParse(form["assetCategoryId"], out int assetCategoryId))
                {
                    AssetCategoryEntity assetCategory = DbContext.AssetCategories.Find(assetCategoryId);
                    assetCategory.Name = form["name"];
                    await DbContext.SaveChangesAsync();
                    return assetCategory;
                }
                else
                {
                    throw new Exception("Не верно задан ID");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        /// <summary>
        /// Удалить запись по ее идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [HttpDelete("{id}")]
        public async Task<ActionResult<AssetCategoryEntity>> Remove(int id)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();
                AssetCategoryEntity assetCategory = await DbContext.AssetCategories.FindAsync(id);
                if (assetCategory != null)
                {
                    DbContext.AssetCategories.Remove(assetCategory);
                    await DbContext.SaveChangesAsync();
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Получить пользователя с ролью администратора
        /// </summary>
        /// <returns></returns>
        private UserEntity GetAuthUserAdmin()
        {
            UserEntity user = DbContext.Users.FirstOrDefault(x => x.Login == User.Identity.Name);
            if (user == null) throw new Exception(String.Format("Пользователь с логином {0} не найден", User.Identity.Name));
            if (user.Role.Name != "Администратор") throw new Exception("Недостаточно прав учетной записи.");
            return user;
        }

    }
}
