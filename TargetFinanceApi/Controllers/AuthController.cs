﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Models;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Controllers
{
    public class AuthController : Controller
    {

        // GET: AuthController
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Авторизация пользователя в системе
        /// </summary>
        /// <param name="loginOrEmail">Логин или адрес элекьтронный почты</param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login(string loginOrEmail, string passwordHash)
        {
            if (loginOrEmail is null) throw new ArgumentNullException(nameof(loginOrEmail));
            if (passwordHash is null) throw new ArgumentNullException(nameof(passwordHash));

            UserEntity user;
            using (Context DbContext = new Context())
            {
                user = DbContext.Users.First<UserEntity>(u => (u.Login == loginOrEmail || u.Email == loginOrEmail) && u.Password == passwordHash);
            }

            bool res = false;
            if (user != null)
            {
                res = true;
            }
            

            return View();
        }

        // GET: AuthController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AuthController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AuthController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AuthController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AuthController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AuthController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AuthController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
