﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TargetFinanceApi;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Controllers
{
    /// <summary>
    /// Управление валютой
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    [EnableCors("_myAllowSpecificOrigins")]
    public class CurrencyController : ControllerBase
    {
        private readonly Context DbContext;

        public CurrencyController(Context context)
        {
            DbContext = context;
        }

        /// <summary>
        /// Получить все записи о валютах
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CurrencyEntity>>> GetAll()
        {
            return await DbContext.Currencies.ToListAsync();
        }


        /// <summary>
        /// Создать новую запись о валюте
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CurrencyEntity>> Create([FromForm] IFormCollection form)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();

                CurrencyEntity currency = new()
                {
                    Name = form["name"],
                    Symbol = form["symbol"]
                };
                DbContext.Currencies.Add(currency);
                await DbContext.SaveChangesAsync();
                return currency;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// Обновить новую запись о валюте
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<CurrencyEntity>> Update([FromForm] IFormCollection form)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();
                if (Int32.TryParse(form["currencyId"], out int currencyId))
                {
                    CurrencyEntity currency = DbContext.Currencies.Find(currencyId);
                    currency.Name = form["name"];
                    currency.Symbol = form["symbol"];
                    await DbContext.SaveChangesAsync();
                    return currency;
                }
                else
                {
                    throw new Exception("Не верно задан ID");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        /// <summary>
        /// Удалить запись о валюте по ее идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<CurrencyEntity>> Remove(int id)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();
                CurrencyEntity currency = await DbContext.Currencies.FindAsync(id);
                if (currency != null)
                {
                    DbContext.Currencies.Remove(currency);
                    await DbContext.SaveChangesAsync();
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// Получить пользователя с ролью администратора
        /// </summary>
        /// <returns></returns>
        private UserEntity GetAuthUserAdmin()
        {
            UserEntity user = DbContext.Users.FirstOrDefault(x => x.Login == User.Identity.Name);
            if (user == null) throw new Exception(String.Format("Пользователь с логином {0} не найден", User.Identity.Name));
            if (user.Role.Name != "Администратор") throw new Exception("Недостаточно прав учетной записи.");
            return user;
        }
    }
}
