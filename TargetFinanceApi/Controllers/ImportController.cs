﻿using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;
using TargetFinanceApi.Models;

namespace TargetFinanceApi.Controllers
{
    /// <summary>
    /// Импорт данных из стороних сервисов
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class ImportController : ControllerBase
    {
        /// <summary>
        /// Контекст работы с базой данных
        /// </summary>
        private readonly Context DbContext;

        /// <summary>
        /// Импорт данных из стороних сервисов
        /// </summary>
        /// <param name="context">Контекст работы с базой данных</param>
        public ImportController(Context context)
        {
            DbContext = context;
        }


        /// <summary>
        /// Загрузка и парсинг файла отчета на сервере
        /// </summary>
        /// <param name="form"></param>
        [ActionName("file")]
        [HttpPost]
        public IActionResult File([FromForm] ImportForm form)
        {
            if (form.FormFile != null)
            {
                if (form.FormFile.Length > 0)
                {
                    string fileName = form.FormFile.FileName;
                    string sizeByte = form.FormFile.Length.ToString();
                    MD5 mD5 = MD5.Create();
                    string hashFile = Convert.ToBase64String(mD5.ComputeHash(Encoding.UTF8.GetBytes(fileName + sizeByte)));
                    UserEntity user = GetAuthUser();
                    ImportEntity logImport = DbContext.LogsImport.FirstOrDefault(x => x.HashFile == hashFile);
                    if (logImport != null) throw new Exception(String.Format("Файл-отчет {0} уже был ранее импортирован пользователм {1} от {2}", fileName, user.Name, logImport.Dt));
                    try
                    {
                        string extension = Path.GetExtension(form.FormFile.FileName);
                        if (extension.ToLower() != ".csv") throw new Exception("Формат файла не соответствует разрешенным: CSV");
                        string pathFile = Path.GetTempFileName();
                        using (FileStream stream = System.IO.File.Create(pathFile)) form.FormFile.CopyTo(stream);
                        ParserInteractiveBrokers parser = new(form.PortfolioId, pathFile, user, ',');
                        // Создание записей логов
                        logImport = new ImportEntity();
                        logImport.HashFile = hashFile;
                        logImport.User = DbContext.Users.Find(user.UserId);
                        logImport.Dt = DateTime.Now;
                        DbContext.LogsImport.Add(logImport);
                        DbContext.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(ex.Message);
                    }
                }
            }
            return Ok("Импорт выполнен успешно.");
        }

        /// <summary>
        /// Получить все записи о импорте для текущего пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ImportEntity>>> GetLogs(int id)
        {
            try
            {
                UserEntity user = GetAuthUser();
                return DbContext.LogsImport.Where(x => x.User.UserId == user.UserId).ToList();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// Удалить запись об импорте
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            try
            {
                ImportEntity import = DbContext.LogsImport.FirstOrDefault(x => x.LogImportId == id);
                if (import != null)
                {
                    DbContext.LogsImport.Remove(import);
                    await DbContext.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    throw new Exception("Запись не найдена");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// Получить текущего авторизованного пользователя
        /// </summary>
        /// <returns></returns>
        private UserEntity GetAuthUser()
        {
            UserEntity user = DbContext.Users.FirstOrDefault(x => x.Login == User.Identity.Name);
            if (user == null) throw new Exception(String.Format("Пользователь с логином {0} не найден", User.Identity.Name));
            return user;
        }



    }
}
