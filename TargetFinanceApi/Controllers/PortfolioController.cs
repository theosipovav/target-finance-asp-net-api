﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TargetFinanceApi;
using TargetFinanceApi.Entities;
using TargetFinanceApi.Models;

namespace TargetFinanceApi.Controllers
{
    /// <summary>
    /// Управление инвестиционными портфелями
    /// </summary>
    [Route("api/[controller]/[action]")]
    [EnableCors("_myAllowSpecificOrigins")]
    [ApiController]
    [Authorize]
    public class PortfolioController : ControllerBase
    {
        /// <summary>
        /// Контекст работы с базой данных
        /// </summary>
        private readonly Context DbContext;

        /// <summary>
        /// Управление инвестиционными портфелями
        /// </summary>
        /// <param name="context">Контекст работы с базой данных</param>
        public PortfolioController(Context context)
        {
            DbContext = context;
        }

        /// <summary>
        /// Получить все портфели текущего авторизованного пользователя
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PortfolioEntitiy>>> GetPortfoliosUser()
        {
            try
            {
                UserEntity user = DbContext.Users.FirstOrDefault(x => x.Login == User.Identity.Name);
                if (user == null) throw new Exception(String.Format("Ошибка авторизации.{0}Пользователь не найден", Environment.NewLine));
                return await DbContext.Portfolios.Where(x => x.User.UserId == user.UserId).ToListAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(new { errorText = ex.Message });
            }
        }

        /// <summary>
        /// Создание нового портфеля
        /// </summary>
        /// <param name="form">Данные формы</param>
        [HttpPost]
        public ActionResult<PortfolioEntitiy> Create([FromForm] PortfolioForm form)
        {
            try
            {
                UserEntity user = DbContext.Users.FirstOrDefault(x => x.Login == User.Identity.Name);
                if (user == null) throw new Exception(String.Format("Пользователь с логином {0} не найден", User.Identity.Name));
                PortfolioTypeEntitiy portfolioType = DbContext.PortfolioTypes.FirstOrDefault(x => x.PortfolioTypeId == form.Type);
                if (portfolioType == null) throw new Exception(String.Format("Тип портфеля и идентификатором ID#{0} не найден", form.Type));
                PortfolioEntitiy portfolio = new PortfolioEntitiy();
                portfolio.Name = form.Name;
                portfolio.CommissionFixed = form.CommissionFixed;
                portfolio.DtOpening = form.DtOpening;
                portfolio.Note = form.Note;
                portfolio.PortfolioType = portfolioType;
                portfolio.User = user;
                DbContext.Portfolios.Add(portfolio);
                DbContext.SaveChanges();
                string portfolioJson = JsonSerializer.Serialize(portfolio);
                return Ok(new { status = "success", message = portfolioJson });
            }
            catch (Exception ex)
            {
                return Ok(new { status = "error", message = ex.Message });
            }
        }

        /// <summary>
        /// Удалить портфель по его идентификатору
        /// </summary>
        /// <param name="id">Идентификатор портфеля</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                UserEntity user = DbContext.Users.FirstOrDefault(x => x.Login == User.Identity.Name);
                if (user == null) throw new Exception(String.Format(
                    "Ошибка авторизации.{0}Пользователь не найден", Environment.NewLine
                ));
                PortfolioEntitiy portfolio = DbContext.Portfolios.FirstOrDefault(x => x.PortfolioId == id && x.User == user);
                if (portfolio == null) throw new Exception(String.Format(
                    "Портфель ID#{0} не найден или не принадлежит пользователю {1} ({2})",
                    id, user.Login, user.Name
                ));
                DbContext.Portfolios.Remove(portfolio);
                await DbContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { errorText = ex.Message });
            }

        }


        /// <summary>
        /// Получить портфель по его идентификатору
        /// </summary>
        /// <param name="id">Идентификатор портфеля</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<PortfolioEntitiy>> GetPortfolioById(int id)
        {
            try
            {
                UserEntity user = await DbContext.Users.FirstOrDefaultAsync(x => x.Login == User.Identity.Name);
                if (user == null) throw new Exception(String.Format(
                    "Ошибка авторизации.{0}Пользователь не найден", Environment.NewLine
                ));
                PortfolioEntitiy portfolio = await DbContext.Portfolios.FirstOrDefaultAsync(x => x.PortfolioId == id && x.User == user);
                if (portfolio == null) throw new Exception(String.Format(
                    "Портфель ID#{0} не найден или не принадлежит пользователю {1} ({2})",
                    id, user.Login, user.Name
                ));
                return portfolio;

            }
            catch (Exception ex)
            {
                return BadRequest(new { errorText = ex.Message });
            }
        }

        /// <summary>
        /// Обновить портфель
        /// </summary>
        /// <param name="form">Данные форма</param>
        [HttpPost]
        public async Task<ActionResult<PortfolioEntitiy>> Update([FromForm] PortfolioUpdateForm form)
        {
            try
            {
                UserEntity user = await DbContext.Users.FirstOrDefaultAsync(x => x.Login == User.Identity.Name);
                if (user == null) throw new Exception(String.Format("Ошибка авторизации.{0}Пользователь не найден", Environment.NewLine));
                PortfolioEntitiy portfolio = await DbContext.Portfolios.FindAsync(form.PortfolioId);
                if (portfolio == null) throw new Exception(String.Format("Портфель ID#{0} не найден или не принадлежит пользователю {1} ({2})", form.PortfolioId, user.Login, user.Name));
                PortfolioTypeEntitiy portfolioType = await DbContext.PortfolioTypes.FindAsync(form.PortfolioTypeId);
                if (portfolioType == null) throw new Exception(String.Format("Тип портфеля ID#{0} не найден", form.PortfolioTypeId));
                portfolio.CommissionFixed = form.CommissionFixed;
                portfolio.DtOpening = form.DtOpening;
                portfolio.Name = form.Name;
                portfolio.Note = form.Note;
                portfolio.PortfolioType = portfolioType;
                DbContext.Entry(portfolio).State = EntityState.Modified;
                await DbContext.SaveChangesAsync();
                return portfolio;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Получить отчет по сделкам, сгруппированных по классам актива
        /// </summary>
        /// <param name="id">Идентификатор портфеля</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<SummaryFromPortfolio>>> GetSummaryFromAssetCategory(int id)
        {
            try
            {
                UserEntity user = await DbContext.Users.FirstOrDefaultAsync(x => x.Login == User.Identity.Name);
                if (user == null) throw new Exception(String.Format(
                    "Ошибка авторизации.{0}Пользователь не найден", Environment.NewLine
                ));
                PortfolioEntitiy portfolio = await DbContext.Portfolios.FirstOrDefaultAsync(x => x.PortfolioId == id && x.User == user);
                if (portfolio == null) throw new Exception(String.Format(
                    "Портфель ID#{0} не найден или не принадлежит пользователю {1} ({2})",
                    id, user.Login, user.Name
                ));
                List<TradeEntity> trades = await DbContext.Trades.Where(x => x.Portfolio == portfolio).ToListAsync();
                List<SummaryFromPortfolio> summary = new List<SummaryFromPortfolio>();
                foreach (IGrouping<AssetCategoryEntity, TradeEntity> group in trades.GroupBy(x => x.AssetCategory))
                {
                    AssetCategoryEntity assetCategory = group.Key;
                    CurrencyEntity currency = null;
                    int count = 0;
                    double priceTransaction = 0;
                    double priceClose = 0;
                    double proceed = 0;
                    foreach (TradeEntity trade in group)
                    {
                        currency = trade.Currency;
                        count += trade.Count;
                        priceTransaction += trade.PriceTransaction;
                        priceClose += trade.PriceClose;
                        proceed += trade.Proceed;
                    }
                    SummaryFromPortfolio summaryAssetCategory = new SummaryFromPortfolio();
                    summaryAssetCategory.AssetCategory = assetCategory;
                    summaryAssetCategory.Count = count;
                    summaryAssetCategory.Currency = currency;
                    summaryAssetCategory.Portfolio = portfolio;
                    summaryAssetCategory.PriceClose = priceClose / group.Count();
                    summaryAssetCategory.PriceTransaction = priceTransaction/ group.Count(); 
                    summaryAssetCategory.Proceed = proceed / group.Count(); 
                    summary.Add(summaryAssetCategory);
                }
                return summary;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Получить отчет по сделкам, сгруппированных по тикерам
        /// </summary>
        /// <param name="id">Идентификатор портфеля</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<SummaryFromPortfolio>>> GetSummaryFromSymbols(int id)
        {
            try
            {
                UserEntity user = await DbContext.Users.FirstOrDefaultAsync(x => x.Login == User.Identity.Name);
                if (user == null) throw new Exception(String.Format(
                    "Ошибка авторизации.{0}Пользователь не найден", Environment.NewLine
                ));
                PortfolioEntitiy portfolio = await DbContext.Portfolios.FirstOrDefaultAsync(x => x.PortfolioId == id && x.User == user);
                if (portfolio == null) throw new Exception(String.Format(
                    "Портфель ID#{0} не найден или не принадлежит пользователю {1} ({2})",
                    id, user.Login, user.Name
                ));
                List<TradeEntity> trades = await DbContext.Trades.Where(x => x.Portfolio == portfolio && x.AssetCategory.Name == "Акции").ToListAsync();
                List<SummaryFromPortfolio> summaryList = new List<SummaryFromPortfolio>();
                foreach (IGrouping<SymbolEntity, TradeEntity> group in trades.GroupBy(x => x.Symbol))
                {
                    SymbolEntity symbol = group.Key;
                    CurrencyEntity currency = null;
                    int count = 0;
                    double priceTransaction = 0;
                    double priceClose = 0;
                    double proceed = 0;
                    foreach (TradeEntity trade in group)
                    {
                        currency = trade.Currency;
                        count += trade.Count;
                        priceTransaction += trade.PriceTransaction;
                        priceClose += trade.PriceClose;
                        proceed += trade.Proceed;
                    }
                    SummaryFromPortfolio summary = new SummaryFromPortfolio();
                    summary.Symbol = symbol;
                    summary.Count = count;
                    summary.Currency = currency;
                    summary.Portfolio = portfolio;
                    summary.PriceClose = priceClose / group.Count();
                    summary.PriceTransaction = priceTransaction / group.Count();
                    summary.Proceed = proceed / group.Count();
                    summaryList.Add(summary);
                }
                return summaryList;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
