﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TargetFinanceApi;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Controllers
{
    /// <summary>
    /// Управление записями о типах портфелей, существующих в системе
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PortfolioTypeController : ControllerBase
    {
        /// <summary>
        /// Контекст работы с базой данных
        /// </summary>
        private readonly Context DbContext;

        /// <summary>
        /// Управление записями о типах портфелей, существующих в системе
        /// </summary>
        /// <param name="context">Контекст работы с базой данных</param>
        public PortfolioTypeController(Context context)
        {
            DbContext = context;
        }


        /// <summary>
        /// Получить все записи
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PortfolioTypeEntitiy>>> GetPortfolioTypes()
        {
            return await DbContext.PortfolioTypes.ToListAsync();
        }

        /// <summary>
        /// Получить запись по ее идентификатору
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PortfolioTypeEntitiy>> GetPortfolioTypeEntitiy(int id)
        {
            var portfolioTypeEntitiy = await DbContext.PortfolioTypes.FindAsync(id);
            if (portfolioTypeEntitiy == null) return NotFound();
            return portfolioTypeEntitiy;
        }

        /// <summary>
        /// Обновить данные записи
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <param name="portfolioTypeEntitiy">Объект типа портфеля</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPortfolioTypeEntitiy(int id, PortfolioTypeEntitiy portfolioTypeEntitiy)
        {
            if (id != portfolioTypeEntitiy.PortfolioTypeId)
            {
                return BadRequest();
            }

            DbContext.Entry(portfolioTypeEntitiy).State = EntityState.Modified;

            try
            {
                await DbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortfolioTypeEntitiyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

       
        /// <summary>
        /// Создать новую запись
        /// </summary>
        /// <param name="portfolioTypeEntitiy"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<PortfolioTypeEntitiy>> PostPortfolioTypeEntitiy(PortfolioTypeEntitiy portfolioTypeEntitiy)
        {
            DbContext.PortfolioTypes.Add(portfolioTypeEntitiy);
            await DbContext.SaveChangesAsync();

            return CreatedAtAction("GetPortfolioTypeEntitiy", new { id = portfolioTypeEntitiy.PortfolioTypeId }, portfolioTypeEntitiy);
        }

        /// <summary>
        /// Удалить запись
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePortfolioTypeEntitiy(int id)
        {
            var portfolioTypeEntitiy = await DbContext.PortfolioTypes.FindAsync(id);
            if (portfolioTypeEntitiy == null)
            {
                return NotFound();
            }

            DbContext.PortfolioTypes.Remove(portfolioTypeEntitiy);
            await DbContext.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Найти записи в системе
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        private bool PortfolioTypeEntitiyExists(int id)
        {
            return DbContext.PortfolioTypes.Any(e => e.PortfolioTypeId == id);
        }
    }
}
