﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TargetFinanceApi;
using TargetFinanceApi.Models;
using TargetFinanceApi.Entities;
using Microsoft.AspNetCore.Authorization;

namespace TargetFinanceApi.Controllers
{
    /// <summary>
    /// Управление ролями пользователей
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RoleController : ControllerBase
    {
        /// <summary>
        /// Контекст работы с базой данных
        /// </summary>
        private readonly Context DBContext;

        /// <summary>
        /// Управление ролями пользователей
        /// </summary>
        /// <param name="context">Контекст работы с базой данных</param>
        public RoleController(Context context)
        {
            DBContext = context;
        }

        /// <summary>
        /// Получить коллекцию всех ролей пользователей
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RoleEntity>>> GetRoles()
        {
            return await DBContext.Roles.ToListAsync();
        }

        /// <summary>
        /// Получить роль по ее идентификатору
        /// </summary>
        /// <param name="id">Идентификатор роли</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<RoleEntity>> One(int id)
        {
            var roleEntity = await DBContext.Roles.FindAsync(id);

            if (roleEntity == null)
            {
                return NotFound();
            }

            return roleEntity;
        }


        /// <summary>
        /// Обновить данные роли по ее идентификатору
        /// </summary>
        /// <param name="id">Идентификатор роли</param>
        /// <param name="roleEntity">Объект роли</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, RoleEntity roleEntity)
        {
            UserEntity admin = GetAuthUserAdmin();
            if (id != roleEntity.RoleId) return BadRequest();
            DBContext.Entry(roleEntity).State = EntityState.Modified;

            try
            {
                await DBContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleEntityExists(id)) return NotFound();
                else throw;
            }

            return NoContent();
        }

        /// <summary>
        /// Создать новую роль
        /// </summary>
        /// <param name="roleEntity">Объект роли</param>
        [HttpPost]
        public async Task<ActionResult<RoleEntity>> Create(RoleEntity roleEntity)
        {
            UserEntity admin = GetAuthUserAdmin();
            DBContext.Roles.Add(roleEntity);
            await DBContext.SaveChangesAsync();

            return CreatedAtAction("GetRoleEntity", new { id = roleEntity.RoleId }, roleEntity);
        }

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <param name="id">Идентификатор роли</param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            UserEntity admin = GetAuthUserAdmin();
            var roleEntity = await DBContext.Roles.FindAsync(id);
            if (roleEntity == null)
            {
                return NotFound();
            }

            DBContext.Roles.Remove(roleEntity);
            await DBContext.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Найти роль в системе по ее идентификатору
        /// <param name="id">Идентификатор роли</param>
        private bool RoleEntityExists(int id)
        {
            return DBContext.Roles.Any(e => e.RoleId == id);
        }


        /// <summary>
        /// Получить пользователя с ролью администратора
        /// </summary>
        /// <returns></returns>
        private UserEntity GetAuthUserAdmin()
        {
            UserEntity user = DBContext.Users.FirstOrDefault(x => x.Login == User.Identity.Name);
            if (user == null) throw new Exception(String.Format("Пользователь с логином {0} не найден", User.Identity.Name));
            if (user.Role.Name != "Администратор") throw new Exception("Недостаточно прав учетной записи.");
            return user;
        }
    }
}
