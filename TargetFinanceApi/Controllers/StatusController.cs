﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TargetFinanceApi;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Controllers
{
    /// <summary>
    /// Управление статусами
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    [EnableCors("_myAllowSpecificOrigins")]
    public class StatusController : ControllerBase
    {
        /// <summary>
        /// Контекст работы с базой данных
        /// </summary>
        private readonly Context DbContext;

        /// <summary>
        /// Управление статусами
        /// </summary>
        /// <param name="context">Контекст работы с базой данных</param>
        public StatusController(Context context)
        {
            DbContext = context;
        }

        /// <summary>
        /// Получить все статусы
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StatusEntity>>> GetAll()
        {
            return await DbContext.Statuses.ToListAsync();
        }

        /// <summary>
        /// Создать новую запись о статусах заказа
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<StatusEntity>> Create([FromForm] IFormCollection form)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();

                StatusEntity statusEntity = new()
                {
                    Name = form["name"],
                    Desc = form["desc"]
                };
                DbContext.Statuses.Add(statusEntity);
                await DbContext.SaveChangesAsync();
                return statusEntity;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Обновить новую запись 
        /// </summary>
        [HttpPut]
        public async Task<ActionResult<StatusEntity>> Update([FromForm] IFormCollection form)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();
                if (Int32.TryParse(form["statusId"], out int statusId))
                {
                    StatusEntity statusEntity = DbContext.Statuses.Find(statusId);
                    statusEntity.Name = form["name"];
                    statusEntity.Desc = form["desc"];
                    await DbContext.SaveChangesAsync();
                    return statusEntity;
                }
                else
                {
                    throw new Exception("Не верно задан ID");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Удалить запись по ее идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [HttpDelete("{id}")]
        public async Task<ActionResult<StatusEntity>> Remove(int id)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();
                StatusEntity status = await DbContext.Statuses.FindAsync(id);
                if (status != null)
                {
                    DbContext.Statuses.Remove(status);
                    await DbContext.SaveChangesAsync();
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Получить пользователя с ролью администратора
        /// </summary>
        private UserEntity GetAuthUserAdmin()
        {
            UserEntity user = DbContext.Users.FirstOrDefault(x => x.Login == User.Identity.Name);
            if (user == null) throw new Exception(String.Format("Пользователь с логином {0} не найден", User.Identity.Name));
            if (user.Role.Name != "Администратор") throw new Exception("Недостаточно прав учетной записи.");
            return user;
        }
    }
}
