﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TargetFinanceApi;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Controllers
{
    /// <summary>
    /// Управление тикерами
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    [EnableCors("_myAllowSpecificOrigins")]
    public class SymbolController : ControllerBase
    {
        /// <summary>
        /// Контекст работы с базой данных
        /// </summary>
        private readonly Context DbContext;

        /// <summary>
        /// Управление тикерами
        /// </summary>
        /// <param name="context">Контекст работы с базой данных</param>
        public SymbolController(Context context)
        {
            DbContext = context;
        }

        /// <summary>
        /// Получить все тикеры
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SymbolEntity>>> GetAll()
        {
            return await DbContext.Symbols.ToListAsync();
        }

        /// <summary>
        /// Создать новую запись
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<SymbolEntity>> Create([FromForm] IFormCollection form)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();
                SymbolEntity symbol = new();
                return symbol;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Обновить новую запись
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<SymbolEntity>> Update([FromForm] IFormCollection form)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();
                SymbolEntity symbol = new();
                return symbol;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Удалить запись по ее идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [HttpDelete("id")]
        public async Task<ActionResult<SymbolEntity>> Remove(int id)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();
                SymbolEntity symbol = await DbContext.Symbols.FindAsync(id);
                if (symbol != null)
                {
                    DbContext.Symbols.Remove(symbol);
                    await DbContext.SaveChangesAsync();
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Получить пользователя с ролью администратора
        /// </summary>
        private UserEntity GetAuthUserAdmin()
        {
            UserEntity user = DbContext.Users.FirstOrDefault(x => x.Login == User.Identity.Name);
            if (user == null) throw new Exception(String.Format("Пользователь с логином {0} не найден", User.Identity.Name));
            if (user.Role.Name != "Администратор") throw new Exception("Недостаточно прав учетной записи.");
            return user;
        }
    }
}
