﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TargetFinanceApi;
using TargetFinanceApi.Entities;
using TargetFinanceApi.Models;

namespace TargetFinanceApi.Controllers
{
    /// <summary>
    /// Управление сделками
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("_myAllowSpecificOrigins")]
    public class TradeController : ControllerBase
    {
        /// <summary>
        /// Контекст работы с базой данных
        /// </summary>
        private readonly Context DbContext;

        /// <summary>
        /// Управление сделками
        /// </summary>
        /// <param name="context"> Контекст работы с базой данных</param>
        public TradeController(Context context)
        {
            DbContext = context;
        }

        /// <summary>
        /// Получить все сделки текущего авторизированного пользователя
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TradeEntity>>> GetTrades()
        {
            try
            {
                UserEntity user = await DbContext.Users.FirstOrDefaultAsync(x => x.Login == User.Identity.Name);
                if (user == null) throw new Exception(String.Format(
                    "Ошибка авторизации.{0}Пользователь не найден", Environment.NewLine
                ));
                return await DbContext.Trades.Where(x => x.Portfolio.User.UserId == user.UserId).ToListAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// Получить сделку по её идентификатору
        /// </summary>
        /// <param name="id">Идентификатор сделки</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<TradeEntity>> GetOneById(int id)
        {
            try
            {
                return this.GetTradeByAuthUser(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Получить коллекцию сделок по портфелю
        /// </summary>
        /// <param name="id">Идентификатор портфеля</param>
        /// <returns>Коллекция объектов TradeEntity</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<TradeEntity>>> GetTradesPortofolio(int id)
        {
            try
            {
                UserEntity user = await DbContext.Users.FirstOrDefaultAsync(x => x.Login == User.Identity.Name);
                if (user == null) throw new Exception(String.Format("Пользователь с логином {0} не найден", User.Identity.Name));
                PortfolioEntitiy portfolio = await DbContext.Portfolios.FirstOrDefaultAsync(x => x.PortfolioId == id && x.User == user);
                if (portfolio == null) throw new Exception(String.Format(
                    "Портфель ID#{0} не найден или не принадлежит пользователю {1} ({2})",
                    id, user.Login, user.Name
                ));
                return DbContext.Trades.Where(x => x.Portfolio.PortfolioId == portfolio.PortfolioId).ToList();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// Обновить данные сделки
        /// </summary>
        /// <param name="form">Данные формы</param>
        [HttpPost]
        public async Task<ActionResult<TradeEntity>> Update([FromForm] TradeForm form)
        {
            try
            {
                TradeEntity trade = this.GetTradeByAuthUser(form.TradeId);
                PortfolioEntitiy portfolio = await DbContext.Portfolios.FindAsync(form.PortfolioId);
                if (portfolio == null) throw new Exception(String.Format(""));
                trade.Portfolio = portfolio;
                AssetCategoryEntity assetCategory = await DbContext.AssetCategories.FindAsync(form.AssetCategoryId);
                if (assetCategory == null) throw new Exception(String.Format(""));
                trade.AssetCategory = assetCategory;
                CurrencyEntity currency = await DbContext.Currencies.FindAsync(form.CurrencyId);
                if (currency == null) throw new Exception(String.Format(""));
                trade.Currency = currency;
                SymbolEntity symbol = await DbContext.Symbols.FindAsync(form.SymbolId);
                if (symbol == null) throw new Exception(String.Format(""));
                trade.Symbol = symbol;
                trade.Dt = form.Dt;
                trade.Count = form.Count;
                trade.PriceTransaction = form.PriceTransaction;
                trade.PriceClose = form.PriceClose;
                trade.Proceed = form.Proceed;
                trade.Commission = form.Commission;
                trade.Basis = form.Basis;
                trade.RealizedPL = form.RealizedPL;
                trade.MtmPL = form.MtmPL;
                StatusEntity status = await DbContext.Statuses.FindAsync(form.StatusId);
                if (status == null) throw new Exception(String.Format(""));
                trade.Status = status;
                DbContext.Entry(trade).State = EntityState.Modified;
                await DbContext.SaveChangesAsync();
                return trade;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Создать новую сделку
        /// </summary>
        /// <param name="form">Данные формы</param>
        [HttpPost]
        public async Task<ActionResult<TradeEntity>> Create([FromForm] TradeForm form)
        {
            try
            {
                TradeEntity trade = new();
                PortfolioEntitiy portfolio = await DbContext.Portfolios.FindAsync(form.PortfolioId);
                if (portfolio == null) throw new Exception(String.Format("Инвестиционный портфель не найден"));
                trade.Portfolio = portfolio;
                AssetCategoryEntity assetCategory = await DbContext.AssetCategories.FindAsync(form.AssetCategoryId);
                if (assetCategory == null) throw new Exception(String.Format("Актив не найден"));
                trade.AssetCategory = assetCategory;
                CurrencyEntity currency = await DbContext.Currencies.FindAsync(form.CurrencyId);
                if (currency == null) throw new Exception(String.Format("Вид валюты не найден"));
                trade.Currency = currency;
                SymbolEntity symbol = await DbContext.Symbols.FindAsync(form.SymbolId);
                if (symbol == null) throw new Exception(String.Format("Тикер акции не найден"));
                trade.Symbol = symbol;
                trade.Dt = form.Dt;
                trade.Count = form.Count;
                trade.PriceTransaction = form.PriceTransaction;
                trade.PriceClose = form.PriceClose;
                trade.Proceed = form.Proceed;
                trade.Commission = form.Commission;
                trade.Basis = form.Basis;
                trade.RealizedPL = form.RealizedPL;
                trade.MtmPL = form.MtmPL;
                StatusEntity status = await DbContext.Statuses.FindAsync(form.StatusId);
                if (status == null) throw new Exception(String.Format("Статус не найден"));
                trade.Status = status;
                DbContext.Add(trade);
                await DbContext.SaveChangesAsync();
                return trade;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Создать новую сделку
        /// </summary>
        /// <param name="tradeEntity">Объект сделки</param>
        [HttpPost]
        public async Task<ActionResult<TradeEntity>> PostTradeEntity(TradeEntity tradeEntity)
        {
            DbContext.Trades.Add(tradeEntity);
            await DbContext.SaveChangesAsync();
            return CreatedAtAction("GetTradeEntity", new { id = tradeEntity.TradeId }, tradeEntity);
        }

        /// <summary>
        /// Удалить сделку по ее идентификатору
        /// </summary>
        /// <param name="id">Идентификатор сделки</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            TradeEntity tradeEntity = this.GetTradeByAuthUser(id);
            DbContext.Trades.Remove(tradeEntity);
            await DbContext.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Получить отчет стоимости всех активов по месяцам в портфели
        /// </summary>
        /// <param name="id">Идентификатор портфеля</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<ModelGroupByMonthForPrice>>> GetGroupByMonthForPrice(int id)
        {
            try
            {
                UserEntity user = this.GetAuthUser();
                PortfolioEntitiy portfolio = await DbContext.Portfolios.FirstOrDefaultAsync(x => x.PortfolioId == id && x.User == user);
                if (portfolio == null) throw new Exception(String.Format(
                    "Портфель ID#{0} не найден или не принадлежит пользователю {1} ({2})",
                    id, user.Login, user.Name
                ));
                List<ModelGroupByMonthForPrice> models = new List<ModelGroupByMonthForPrice>();
                List<TradeEntity> trades = DbContext.Trades.Where(x => x.Portfolio == portfolio && x.Status.Name != "Отмена").ToList();
                List<TradeEntity> tradesGroup = new List<TradeEntity>();
                foreach (IGrouping<DateTime, TradeEntity> group in trades.GroupBy(x => x.Dt))
                {
                    DateTime dt = group.Key;
                    if (models.Count > 0)
                    {
                        if (dt.Month > models.Last().Dt.Month && dt.Year >= models.Last().Dt.Year)
                        {
                            models.Add(new ModelGroupByMonthForPrice(new DateTime(dt.Year, dt.Month, 1), tradesGroup));
                            tradesGroup = new List<TradeEntity>();
                        }
                        else
                        {
                            tradesGroup.AddRange(group.ToList());
                        }
                    }
                    else
                    {
                        models.Add(new ModelGroupByMonthForPrice(new DateTime(dt.Year, dt.Month, 1), tradesGroup));
                        tradesGroup = new List<TradeEntity>();
                    }
                }
                return models;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Получить сделку авторизованного пользователя по её идентификатору
        /// </summary>
        /// <param name="id">Идентификатор сделки</param>
        /// <returns></returns>
        private TradeEntity GetTradeByAuthUser(int id)
        {
            UserEntity user = this.GetAuthUser();
            TradeEntity trade = DbContext.Trades.FirstOrDefault(x => x.Portfolio.User.UserId == user.UserId && x.TradeId == id);
            if (trade == null) throw new Exception(String.Format("Сделка с идентификатором {0} не найдена", id));
            if (trade.Portfolio.User.UserId != user.UserId) throw new Exception(String.Format("Данная сделка не принадлежит пользователю {0} ({1})", user.Login, user.Name));
            return trade;
        }

        /// <summary>
        /// Получить текущего авторизованного пользователями в системе
        /// </summary>
        private UserEntity GetAuthUser()
        {
            UserEntity user = DbContext.Users.FirstOrDefault(x => x.Login == User.Identity.Name);
            if (user == null) throw new Exception(String.Format("Пользователь с логином {0} не найден", User.Identity.Name));
            return user;
        }
    }
}
