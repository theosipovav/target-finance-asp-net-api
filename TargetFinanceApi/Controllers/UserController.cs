﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TargetFinanceApi.Models;
using TargetFinanceApi.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace TargetFinanceApi.Controllers
{
    /// <summary>
    /// Управление пользователями 
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    [EnableCors("_myAllowSpecificOrigins")]
    public class UserController : ControllerBase
    {
        /// <summary>
        /// Контекст работы с базой данных
        /// </summary>
        private readonly Context DbContext;

        /// <summary>
        /// Управление пользователями 
        /// </summary>
        /// <param name="context">Контекст работы с базой данных</param>
        public UserController(Context context)
        {
            DbContext = context;
        }

        /// <summary>
        /// Получить коллекцию всех пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserEntity>>> All()
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();
                return await DbContext.Users.ToListAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Удалить пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();
                UserEntity user = await DbContext.Users.FindAsync(id);
                if (admin.UserId == user.UserId) throw new Exception("Удаление самого себя запрещено.");
                DbContext.Users.Remove(user);
                await DbContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Выполнить сброс пароля
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> ResetPassword(int id)
        {
            try
            {
                UserEntity admin = this.GetAuthUserAdmin();
                UserEntity user = await DbContext.Users.FindAsync(id);
                user.Password = "";
                DbContext.Entry(user).State = EntityState.Modified;
                await DbContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Получить пользователя по его идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<UserEntity>> One(int id)
        {
            var userEntity = await DbContext.Users.FindAsync(id);
            if (userEntity == null) return NotFound();
            return userEntity;
        }

        /// <summary>
        /// Получить пользователя по его логину
        /// </summary>
        /// <param name="login">Логин</param>
        /// <returns></returns>
        [HttpGet("login")]
        public async Task<ActionResult<UserEntity>> OneForLogin(string login)
        {
            try
            {
                UserEntity user = await DbContext.Users.FirstOrDefaultAsync(x => x.Login == login);
                if (user == null) throw new Exception(String.Format("Пользователь с логином {0} не найден.", login));
                return user;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Получить пользователя по логину и паролю
        /// </summary>
        /// <param name="login">Логин</param>
        /// <param name="password">Пароль</param>
        [HttpGet("{login}/{password}")]
        [EnableCors("_myAllowSpecificOrigins")]
        public async Task<ActionResult<UserEntity>> GetUser(String login, String password)
        {
            UserEntity user = await DbContext.Users.FirstOrDefaultAsync(u => u.Login == login && u.Password == password);
            if (user == null) return NotFound();
            return user;
        }

        /// <summary>
        /// Обновить учетную запись пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <param name="userEntity">Объект пользователя</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, UserEntity userEntity)
        {
            if (id != userEntity.UserId) return BadRequest();
            DbContext.Entry(userEntity).State = EntityState.Modified;
            try
            {
                await DbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
       
        /// <summary>
        /// Создать новую учетную запись
        /// </summary>
        /// <param name="userEntity"></param>
        [HttpPost]
        public async Task<ActionResult<UserEntity>> Create([FromForm] IFormCollection form)
        {
            UserEntity user = new UserEntity();
            DbContext.Users.Add(user);
            await DbContext.SaveChangesAsync();
            return Ok(user);
        }

        /// <summary>
        /// Удалить запись пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserEntity(int id)
        {
            var userEntity = await DbContext.Users.FindAsync(id);
            if (userEntity == null)
            {
                return NotFound();
            }
            DbContext.Users.Remove(userEntity);
            await DbContext.SaveChangesAsync();
            return NoContent();
        }

        /// <summary>
        /// Найти существующего пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns></returns>
        private bool UserEntityExists(int id)
        {
            return DbContext.Users.Any(e => e.UserId == id);
        }

        /// <summary>
        /// Получить пользователя с ролью администратора
        /// </summary>
        /// <returns></returns>
        private UserEntity GetAuthUserAdmin()
        {
            UserEntity user = DbContext.Users.FirstOrDefault(x => x.Login == User.Identity.Name);
            if (user == null) throw new Exception(String.Format("Пользователь с логином {0} не найден", User.Identity.Name));
            if (user.Role.Name != "Администратор") throw new Exception("Недостаточно прав учетной записи.");
            return user;
        }
    }
}
