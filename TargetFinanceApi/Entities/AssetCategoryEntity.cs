﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Entities
{
    /// <summary>
    /// Класс актива
    /// </summary>
    [Table("asset_category")]
    [Comment("Класс актива")]
    public class AssetCategoryEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [Column("asset_category_id")]
        [Comment("Идентификатор")]
        public int AssetCategoryId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [Required]
        [Comment("Наименование")]
        [Column("name")]
        [MaxLength(250)]
        public string Name { get; set; }

    }
}
