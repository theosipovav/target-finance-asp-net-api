﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TargetFinanceApi.Entities
{
    /// <summary>
    /// Класс актива
    /// </summary>
    [Table("class_active")]
    [Comment("Класс актива")]
    public class ClassActiveEntity
    {
        /// <summary>
        /// Идентификатор сделки
        /// </summary>
        [Key]
        [Column("class_active_id")]
        [Comment("Идентификатор")]
        public int ClassActiveId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [Column("name")]
        [Comment("Наименование")]
        public string Name { get; set; }
    }
}
