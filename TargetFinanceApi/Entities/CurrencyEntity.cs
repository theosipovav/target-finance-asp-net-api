﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TargetFinanceApi.Entities
{
    /// <summary>
    /// Валюта
    /// </summary>
    [Table("currency")]
    [Comment("Валюта")]
    public class CurrencyEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [Column("currency_id")]
        [Comment("Идентификатор")]
        public int CurrencyId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [Comment("Наименование")]
        [Column("name")]
        [MaxLength(100)]
        public String Name { get; set; }

        /// <summary>
        /// Обозначение
        /// </summary>
        [Comment("Обозначение")]
        [Column("symbol")]
        [MaxLength(3)]
        public String Symbol { get; set; }

    }
}
