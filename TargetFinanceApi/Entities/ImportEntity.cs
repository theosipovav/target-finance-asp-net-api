﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Entities
{
    /// <summary>
    /// Лог импорта отчета в систему
    /// </summary>
    [Table("log_mport")]
    [Comment("Сделка")]
    public class ImportEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [Column("log_mport_id")]
        [Comment("Идентификатор")]
        public int LogImportId { get; set; }

        /// <summary>
        /// Дата импорта
        /// </summary>
        [Column("dt")]
        public DateTime Dt { get; set; }


        /// <summary>
        /// Пользователь, от которого имени был произведен импорт
        /// </summary>
        [Column("user_id")]
        [Comment("Пользователь, от которого имени был произведен импорт")]
        public virtual UserEntity User { get; set; }

        /// <summary>
        /// ХЭШ загруженного файла-отчета
        /// </summary>
        [Column("hash_file")]
        [Comment("ХЭШ загруженного файла-отчета")]
        [Required]
        public string HashFile { get; set; }
    }
}
