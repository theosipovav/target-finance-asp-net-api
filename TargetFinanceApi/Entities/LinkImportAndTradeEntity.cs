﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Entities
{
    /// <summary>
    /// Связь импорта и загруженных сделок
    /// </summary>
    [Table("log_import_link")]
    [Comment("Связь импорта и загруженных сделок")]
    public class LinkImportAndTradeEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [Column("log_import_link_id")]
        [Comment("Идентификатор")]
        public int LogImportLinkId { get; set; }

        /// <summary>
        /// Запись импорта
        /// </summary>
        [ForeignKey("log_import")]
        [Comment("Запись импорта")]
        [Required]
        public virtual ImportEntity LogImport { get; set; }

        /// <summary>
        /// Загруженная сделка
        /// </summary>
        [ForeignKey("trade_id")]
        [Comment("Загруженная сделка")]
        [Required]
        public virtual TradeEntity Trade { get; set; }
     }
}
