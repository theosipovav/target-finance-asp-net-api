﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Models;

namespace TargetFinanceApi.Entities
{
    /// <summary>
    /// Портфель
    /// </summary>
    [Table("portfolio")]
    [Comment("Портфель")]
    public class PortfolioEntitiy
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [Column("portfolio_id")]
        [Comment("Идентификатор")]
        public int PortfolioId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [Column("name")]
        [Comment("Наименование")]
        [MaxLength(250)]
        public string Name { get; set; }

        /// <summary>
        /// Фиксированная комиссия
        /// </summary>
        [Comment("Фиксированная комиссия")]
        [Column("commission_fixed")]
        public float CommissionFixed { get; set; }

        /// <summary>
        /// Дата открытия
        /// </summary>
        [Column("dt_opening")]
        [Comment("Дата открытия")]
        public DateTime DtOpening { get; set; }

        /// <summary>
        /// Заметка
        /// </summary>
        [Column("note")]
        [Comment("Заметка")]
        public string Note { get; set; }



        /// <summary>
        /// Тип портфеля
        /// </summary>
        [ForeignKey("portfolio_type_id")]
        [Comment("Идентификатор типа портфеля")]
        public virtual PortfolioTypeEntitiy PortfolioType { get; set; }

        /// <summary>
        /// Пользователь, владеющий данным портфелем
        /// </summary>
        [ForeignKey("user_id")]
        [Comment("Идентификатор пользователя, владеющий данным портфелем")]
        public virtual UserEntity User { get; set; }
    }
}
