﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TargetFinanceApi.Entities
{
    /// <summary>
    /// Тип портеля
    /// </summary>
    [Table("portfolio_type")]
    [Comment("Тип портеля")]
    public class PortfolioTypeEntitiy
    {

        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [Column("portfolio_type_id")]
        [Comment("Идентификатор")]
        public int PortfolioTypeId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(100)]
        [Column("name")]
        [Comment("Наименование")]
        public String Name { get; set; }
    }
}
