﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TargetFinanceApi.Entities
{
    /// <summary>
    /// Роль пользователя в системе
    /// </summary>
    [Table("role")]
    [Comment("Роль пользователя в системе")]
    public class RoleEntity
    {
        /// <summary>
        /// Идентификатор роли
        /// </summary>
        [Key]
        [Column("role_id")]
        [Required]
        [Comment("Идентификатор роли")]
        public int RoleId { get; set; }

        /// <summary>
        /// Наименование роли (экранное имя)
        /// </summary>
        [Required]
        [Comment("Наименование роли")]
        [Column("name")]
        [MaxLength(150)]
        public string Name { get; set; }

    }
}
