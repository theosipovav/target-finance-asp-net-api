﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TargetFinanceApi.Entities
{
    /// <summary>
    /// Класс актива
    /// </summary>
    [Table("status")]
    [Comment("Статусы")]
    public class StatusEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [Column("status_id")]
        [Comment("Идентификатор")]
        public int StatusId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [Column("name")]
        [Comment("Наименование")]
        [Required]
        public string Name { get; set; }


        /// <summary>
        /// Наименование
        /// </summary>
        [Column("desc")]
        [Comment("Описание")]
        public string Desc { get; set; }

    }
}
