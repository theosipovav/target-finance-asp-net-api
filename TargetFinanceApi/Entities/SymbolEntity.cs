﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Entities
{
    /// <summary>
    /// Символ
    /// </summary>
    [Table("symbol")]
    [Comment("Тикеры")]
    public class SymbolEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [Column("symbol_id")]
        [Comment("Идентификатор")]
        public int SymbolId { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        [Required]
        [Comment("Значение")]
        [Column("value")]
        [MaxLength(250)]
        public string Value { get; set; }


        /// <summary>
        /// Описание
        /// </summary>
        [Comment("Описание")]
        [Column("desc")]
        [MaxLength(250)]
        public string Desc { get; set; }
    }
}
