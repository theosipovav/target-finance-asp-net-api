﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Entities
{
    /// <summary>
    /// Сделка
    /// </summary>
    [Table("trade")]
    [Comment("Сделка")]
    public class TradeEntity
    {
        [Key]
        [Column("trade_id")]
        [Comment("Идентификатор")]
        public int TradeId { get; set; }

        /// <summary>
        /// Портфель
        /// </summary>
        [ForeignKey("portfolio_id")]
        [Comment("Идентфикатор портфеля")]
        [Required]
        public virtual PortfolioEntitiy Portfolio { get; set; }

        /// <summary>
        /// Класс актива
        /// </summary>
        [ForeignKey("asset_category_id")]
        [Comment("Идентфиикатор класса актива")]
        [Required]
        public virtual AssetCategoryEntity AssetCategory { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        [ForeignKey("currency_id")]
        [Comment("Идентфиикатор валюты")]
        [Required]
        public virtual CurrencyEntity Currency { get; set; }


        /// <summary>
        /// Символ
        /// </summary>
        [ForeignKey("symbol_id")]
        [Comment("Идентфиикатор символа")]
        [Required]
        public virtual SymbolEntity Symbol { get; set; }

        /// <summary>
        /// Дата/Время
        /// </summary>
        [Column("dt")]
        [Comment("Дата/Время")]
        [Required]
        public DateTime Dt { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        [Column("count")]
        [Comment("Количество")]
        [Required]
        public int Count { get; set; }

        /// <summary>
        /// Цена транзакции
        /// </summary>
        [Column("price_transaction")]
        [Comment("Цена транзакции")]
        [Required]
        public double PriceTransaction { get; set; }

        /// <summary>
        /// Цена закрытия
        /// </summary>
        [Column("price_close")]
        [Comment("Цена закрытия")]
        [Required]
        public double PriceClose { get; set; }

        /// <summary>
        /// Выручка
        /// </summary>
        [Column("proceed")]
        [Comment("Выручка")]
        [Required]
        public double Proceed { get; set; }

        /// <summary>
        /// Комиссия
        /// </summary>
        [Column("commission")]
        [Comment("Комиссия")]
        [Required]
        public double Commission { get; set; }

        /// <summary>
        /// Базис
        /// </summary>
        [Column("basic")]
        [Comment("Базис")]
        [Required]
        public double Basis { get; set; }

        /// <summary>
        /// Реализованная П/У
        /// </summary>
        [Column("realized_pl")]
        [Comment("Реализованная П/У")]
        [Required]
        public double RealizedPL { get; set; }

        /// <summary>
        /// Рыноч. переоценка П/У
        /// </summary>
        [Column("mtm_pl")]
        [Comment("Рыноч.переоценка П/У")]
        [Required]
        public double MtmPL { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        [ForeignKey("status_id")]
        [Comment("Идентфиикатор статуса")]
        [Required]
        public virtual StatusEntity Status { get; set; }
    }
}
