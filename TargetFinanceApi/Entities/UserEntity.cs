﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Entities
{
    /// <summary>
    /// Пользователь
    /// </summary>
    [Table("user")]
    [Comment("Пользователи системы")]
    public class UserEntity
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        [Key]
        [Column("user_id")]
        [Comment("Идентификатор пользователя")]
        public int UserId { get; set; }

        /// <summary>
        /// Логин для входа в систему
        /// </summary>
        [Required]
        [Comment("Логин для входа в систему")]
        [Column("login")]
        [MaxLength(250)]
        public string Login { get; set; }
        /// <summary>
        /// Пароль для входа в систему
        /// </summary>
        [Required]
        [Comment("Пароль для входа в систему")]
        [Column("password")]
        [MaxLength(250)]
        public string Password { get; set; }
        /// <summary>
        /// Адрес электронной почты
        /// </summary>
        [Required]
        [Comment("Адрес электронной почты")]
        [Column("email")]
        [MaxLength(250)]
        public string Email { get; set; }
        /// <summary>
        /// ФИО пользователя
        /// </summary>
        [Required]
        [Comment("ФИО пользователя")]
        [Column("name")]
        [MaxLength(250)]
        public string Name { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        [Required]
        [Column("dt_registration")]
        [Comment("Дата регистрации")]
        public DateTime DtRegistration { get; set; }

        /// <summary>
        /// Роль пользователя в системе
        /// </summary
        /// 
        [ForeignKey("role_id")]
        [Required]
        [Comment("Роль пользователя в системе")]
        public virtual RoleEntity Role { get; set; }
    }
}
