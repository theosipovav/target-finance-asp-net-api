﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TargetFinanceApi.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "asset_category",
                columns: table => new
                {
                    asset_category_id = table.Column<int>(type: "int", nullable: false, comment: "Идентификатор")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false, comment: "Наименование")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_asset_category", x => x.asset_category_id);
                },
                comment: "Класс актива");

            migrationBuilder.CreateTable(
                name: "class_active",
                columns: table => new
                {
                    class_active_id = table.Column<int>(type: "int", nullable: false, comment: "Идентификатор")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "Наименование")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_class_active", x => x.class_active_id);
                },
                comment: "Класс актива");

            migrationBuilder.CreateTable(
                name: "currency",
                columns: table => new
                {
                    currency_id = table.Column<int>(type: "int", nullable: false, comment: "Идентификатор")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "Наименование"),
                    symbol = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: true, comment: "Обозначение")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_currency", x => x.currency_id);
                },
                comment: "Валюта");

            migrationBuilder.CreateTable(
                name: "portfolio_type",
                columns: table => new
                {
                    portfolio_type_id = table.Column<int>(type: "int", nullable: false, comment: "Идентификатор")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "Наименование")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_portfolio_type", x => x.portfolio_type_id);
                },
                comment: "Тип портеля");

            migrationBuilder.CreateTable(
                name: "role",
                columns: table => new
                {
                    role_id = table.Column<int>(type: "int", nullable: false, comment: "Идентификатор роли")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false, comment: "Наименование роли")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role", x => x.role_id);
                },
                comment: "Роль пользователя в системе");

            migrationBuilder.CreateTable(
                name: "status",
                columns: table => new
                {
                    status_id = table.Column<int>(type: "int", nullable: false, comment: "Идентификатор")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: false, comment: "Наименование"),
                    desc = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "Описание")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_status", x => x.status_id);
                },
                comment: "Статусы");

            migrationBuilder.CreateTable(
                name: "symbol",
                columns: table => new
                {
                    symbol_id = table.Column<int>(type: "int", nullable: false, comment: "Идентификатор")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    value = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false, comment: "Значение"),
                    desc = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true, comment: "Описание")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_symbol", x => x.symbol_id);
                },
                comment: "Тикеры");

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    user_id = table.Column<int>(type: "int", nullable: false, comment: "Идентификатор пользователя")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    login = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false, comment: "Логин для входа в систему"),
                    password = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false, comment: "Пароль для входа в систему"),
                    email = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false, comment: "Адрес электронной почты"),
                    name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false, comment: "ФИО пользователя"),
                    dt_registration = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "getdate()", comment: "Дата регистрации"),
                    role_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.user_id);
                    table.ForeignKey(
                        name: "FK_user_role_role_id",
                        column: x => x.role_id,
                        principalTable: "role",
                        principalColumn: "role_id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "Пользователи системы");

            migrationBuilder.CreateTable(
                name: "log_mport",
                columns: table => new
                {
                    log_mport_id = table.Column<int>(type: "int", nullable: false, comment: "Идентификатор")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    dt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: true),
                    hash_file = table.Column<string>(type: "nvarchar(max)", nullable: false, comment: "ХЭШ загруженного файла-отчета")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_log_mport", x => x.log_mport_id);
                    table.ForeignKey(
                        name: "FK_log_mport_user_UserId",
                        column: x => x.UserId,
                        principalTable: "user",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                },
                comment: "Сделка");

            migrationBuilder.CreateTable(
                name: "portfolio",
                columns: table => new
                {
                    portfolio_id = table.Column<int>(type: "int", nullable: false, comment: "Идентификатор")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true, comment: "Наименование"),
                    commission_fixed = table.Column<float>(type: "real", nullable: false, comment: "Фиксированная комиссия"),
                    dt_opening = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "getdate()", comment: "Дата открытия"),
                    note = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "Заметка"),
                    portfolio_type_id = table.Column<int>(type: "int", nullable: true),
                    user_id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_portfolio", x => x.portfolio_id);
                    table.ForeignKey(
                        name: "FK_portfolio_portfolio_type_portfolio_type_id",
                        column: x => x.portfolio_type_id,
                        principalTable: "portfolio_type",
                        principalColumn: "portfolio_type_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_portfolio_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                },
                comment: "Портфель");

            migrationBuilder.CreateTable(
                name: "trade",
                columns: table => new
                {
                    trade_id = table.Column<int>(type: "int", nullable: false, comment: "Идентификатор")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    portfolio_id = table.Column<int>(type: "int", nullable: false),
                    asset_category_id = table.Column<int>(type: "int", nullable: false),
                    currency_id = table.Column<int>(type: "int", nullable: false),
                    symbol_id = table.Column<int>(type: "int", nullable: false),
                    dt = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "Дата/Время"),
                    count = table.Column<int>(type: "int", nullable: false, comment: "Количество"),
                    price_transaction = table.Column<double>(type: "float", nullable: false, comment: "Цена транзакции"),
                    price_close = table.Column<double>(type: "float", nullable: false, comment: "Цена закрытия"),
                    proceed = table.Column<double>(type: "float", nullable: false, comment: "Выручка"),
                    commission = table.Column<double>(type: "float", nullable: false, comment: "Комиссия"),
                    basic = table.Column<double>(type: "float", nullable: false, comment: "Базис"),
                    realized_pl = table.Column<double>(type: "float", nullable: false, comment: "Реализованная П/У"),
                    mtm_pl = table.Column<double>(type: "float", nullable: false, comment: "Рыноч.переоценка П/У"),
                    status_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_trade", x => x.trade_id);
                    table.ForeignKey(
                        name: "FK_trade_asset_category_asset_category_id",
                        column: x => x.asset_category_id,
                        principalTable: "asset_category",
                        principalColumn: "asset_category_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_trade_currency_currency_id",
                        column: x => x.currency_id,
                        principalTable: "currency",
                        principalColumn: "currency_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_trade_portfolio_portfolio_id",
                        column: x => x.portfolio_id,
                        principalTable: "portfolio",
                        principalColumn: "portfolio_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_trade_status_status_id",
                        column: x => x.status_id,
                        principalTable: "status",
                        principalColumn: "status_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_trade_symbol_symbol_id",
                        column: x => x.symbol_id,
                        principalTable: "symbol",
                        principalColumn: "symbol_id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "Сделка");

            migrationBuilder.CreateTable(
                name: "log_import_link",
                columns: table => new
                {
                    log_import_link_id = table.Column<int>(type: "int", nullable: false, comment: "Идентификатор")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    log_import = table.Column<int>(type: "int", nullable: false),
                    trade_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_log_import_link", x => x.log_import_link_id);
                    table.ForeignKey(
                        name: "FK_log_import_link_log_mport_log_import",
                        column: x => x.log_import,
                        principalTable: "log_mport",
                        principalColumn: "log_mport_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_log_import_link_trade_trade_id",
                        column: x => x.trade_id,
                        principalTable: "trade",
                        principalColumn: "trade_id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "Связь импорта и загруженных сделок");

            migrationBuilder.InsertData(
                table: "currency",
                columns: new[] { "currency_id", "name", "symbol" },
                values: new object[,]
                {
                    { 1, "Российский рубль", "RUB" },
                    { 2, "ЕВРО", "EUR" },
                    { 3, "Доллар США", "USD" }
                });

            migrationBuilder.InsertData(
                table: "portfolio_type",
                columns: new[] { "portfolio_type_id", "name" },
                values: new object[] { 1, "Стандарт" });

            migrationBuilder.InsertData(
                table: "role",
                columns: new[] { "role_id", "name" },
                values: new object[,]
                {
                    { 1, "Администратор" },
                    { 2, "Пользователь" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_log_import_link_log_import",
                table: "log_import_link",
                column: "log_import");

            migrationBuilder.CreateIndex(
                name: "IX_log_import_link_trade_id",
                table: "log_import_link",
                column: "trade_id");

            migrationBuilder.CreateIndex(
                name: "IX_log_mport_UserId",
                table: "log_mport",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_portfolio_portfolio_type_id",
                table: "portfolio",
                column: "portfolio_type_id");

            migrationBuilder.CreateIndex(
                name: "IX_portfolio_user_id",
                table: "portfolio",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_trade_asset_category_id",
                table: "trade",
                column: "asset_category_id");

            migrationBuilder.CreateIndex(
                name: "IX_trade_currency_id",
                table: "trade",
                column: "currency_id");

            migrationBuilder.CreateIndex(
                name: "IX_trade_portfolio_id",
                table: "trade",
                column: "portfolio_id");

            migrationBuilder.CreateIndex(
                name: "IX_trade_status_id",
                table: "trade",
                column: "status_id");

            migrationBuilder.CreateIndex(
                name: "IX_trade_symbol_id",
                table: "trade",
                column: "symbol_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_role_id",
                table: "user",
                column: "role_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "class_active");

            migrationBuilder.DropTable(
                name: "log_import_link");

            migrationBuilder.DropTable(
                name: "log_mport");

            migrationBuilder.DropTable(
                name: "trade");

            migrationBuilder.DropTable(
                name: "asset_category");

            migrationBuilder.DropTable(
                name: "currency");

            migrationBuilder.DropTable(
                name: "portfolio");

            migrationBuilder.DropTable(
                name: "status");

            migrationBuilder.DropTable(
                name: "symbol");

            migrationBuilder.DropTable(
                name: "portfolio_type");

            migrationBuilder.DropTable(
                name: "user");

            migrationBuilder.DropTable(
                name: "role");
        }
    }
}
