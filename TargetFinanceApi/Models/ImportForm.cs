﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TargetFinanceApi.Models
{
    /// <summary>
    /// Модель данных формы импорта файла
    /// </summary>
    public class ImportForm
    {
        /// <summary>
        /// Идентификатор портфеля
        /// </summary>
        public int PortfolioId { get; set; }

        /// <summary>
        /// Файл 
        /// </summary>
        public IFormFile FormFile { get; set; }
    }
}
