﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Models
{
    /// <summary>
    /// Модель отчета сделок по месяцам и стоимости
    /// </summary>
    public class ModelGroupByMonthForPrice
    {
        /// <summary>
        /// Дата
        /// </summary>
        public DateTime Dt { get; set; }

        /// <summary>
        /// Коллекция сделок
        /// </summary>
        public IEnumerable<TradeEntity> Trades { get; set; }

        /// <summary>
        /// Создать модель отчета сделок по месяцам и стоимости
        /// </summary>
        /// <param name="dt">Дата</param>
        /// <param name="trades">Коллекция сделок</param>
        public ModelGroupByMonthForPrice(DateTime dt, IEnumerable<TradeEntity> trades)
        {
            this.Dt = dt;
            Trades = trades;
        }
    }
}
