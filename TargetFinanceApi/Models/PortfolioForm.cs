﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TargetFinanceApi.Models
{
    /// <summary>
    /// Модель данных формы создания портфеля
    /// </summary>
    public class PortfolioForm
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Тип портфеля
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// Фиксированная комиссия
        /// </summary>
        public float CommissionFixed { get; set; }

        /// <summary>
        /// Дата открытия
        /// </summary>
        public DateTime DtOpening { get; set; }

        /// <summary>
        /// Заметка
        /// </summary>
        public string Note { get; set; }
    }
}
