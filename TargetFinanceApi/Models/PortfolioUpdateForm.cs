﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TargetFinanceApi.Models
{
    /// <summary>
    /// Модель данных формы обновленяи портфеля
    /// </summary>
    public class PortfolioUpdateForm
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int PortfolioId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>

        public string Name { get; set; }

        /// <summary>
        /// Фиксированная комиссия
        /// </summary>

        public float CommissionFixed { get; set; }

        /// <summary>
        /// Дата открытия
        /// </summary>
        public DateTime DtOpening { get; set; }

        /// <summary>
        /// Заметка
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Идентификатор типа портфеля
        /// </summary>
        public int PortfolioTypeId { get; set; }
    }
}
