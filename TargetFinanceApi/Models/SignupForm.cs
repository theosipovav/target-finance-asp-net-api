﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TargetFinanceApi.Models
{
    /// <summary>
    /// Модель данных формы регистрации в системе
    /// </summary>
    public class SignupForm
    {
        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string PasswordRepeat { get; set; }

        /// <summary>
        /// Электронный адрес
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// ФИО пользователя
        /// </summary>
        public string Name { get; set; }
    }
}
