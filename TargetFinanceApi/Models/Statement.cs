﻿using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TargetFinanceApi.Models
{

    /// <summary>
    /// Statement,Header,Название поля,Значение поля
    /// </summary>
    public class Statement
    {
        [Index(0)]
        public string Table { get; set; }

        [Index(1)]
        public string Header { get; set; }

        [Index(2)]
        public string Name { get; set; }


        [Index(3)]
        public string Value { get; set; }
    }

}
