﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi.Models
{
    /// <summary>
    /// Модель отчета сделок по портфелю
    /// </summary>
    public class SummaryFromPortfolio
    {
        /// <summary>
        /// Портфель
        /// </summary>
        public PortfolioEntitiy Portfolio { get; set; }

        /// <summary>
        /// Тикер
        /// </summary>
        public SymbolEntity Symbol { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        public CurrencyEntity Currency { get; set; }

        /// <summary>
        /// Класс актива
        /// </summary>
        public AssetCategoryEntity AssetCategory { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Средняя цена транзакции
        /// </summary>
        public double PriceTransaction { get; set; }

        /// <summary>
        /// Средняя цена закрытия
        /// </summary>
        public double PriceClose { get; set; }

        /// <summary>
        /// Средняя выручка
        /// </summary>
        public double Proceed { get; set; }
    }
}
