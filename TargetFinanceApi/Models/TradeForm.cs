﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TargetFinanceApi.Models
{
    /// <summary>
    /// Модель данных формы для создания и обновления сделки
    /// </summary>
    public class TradeForm
    {
        /// <summary>
        /// Идентификатор сделки
        /// </summary>
        public int TradeId { get; set; }

        /// <summary>
        /// Идентификатор портфеля
        /// </summary>
        public int PortfolioId  { get; set; }

        /// <summary>
        /// Идентфиикатор класса актива
        /// </summary>
        public int AssetCategoryId { get; set; }

        /// <summary>
        /// Идентификатор валюты
        /// </summary>
        public int CurrencyId { get; set; }


        /// <summary>
        /// Идентификатор тикера
        /// </summary>
        public int SymbolId { get; set; }

        /// <summary>
        /// Дата/Время
        /// </summary>
        public DateTime Dt { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Цена транзакции
        /// </summary>
        public double PriceTransaction { get; set; }

        /// <summary>
        /// Цена закрытия
        /// </summary>
        public double PriceClose { get; set; }

        /// <summary>
        /// Выручка
        /// </summary>
        public double Proceed { get; set; }

        /// <summary>
        /// Комиссия
        /// </summary>
        public double Commission { get; set; }

        /// <summary>
        /// Базис
        /// </summary>
        public double Basis { get; set; }

        /// <summary>
        /// Реализованная П/У
        /// </summary>
        public double RealizedPL { get; set; }

        /// <summary>
        /// Рыноч. переоценка П/У
        /// </summary>
        public double MtmPL { get; set; }

        /// <summary>
        /// Идентфиикатор статуса
        /// </summary>
        public int StatusId { get; set; }
    }
}
