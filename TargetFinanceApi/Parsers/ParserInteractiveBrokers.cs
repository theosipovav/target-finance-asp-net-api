﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TargetFinanceApi.Entities;

namespace TargetFinanceApi
{
    public class ParserInteractiveBrokers
    {
        /// <summary>
        /// Идентификатор портфеля
        /// </summary>
        private int PortfolioId { get; set; }

        private List<string[]> ContentRows { get; set; }



        /// <summary>
        /// Пользователь, от которого имени осуществляется импорт
        /// </summary>
        private UserEntity User { get; set; }

        /// <summary>
        /// Флаг ошибки 
        /// </summary>
        public bool IsWarning { get; set; }
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public List<string> MessagesWarning { get; set; }


        public ParserInteractiveBrokers(int portfolioId, string pathFile, UserEntity user, char separator = ',')
        {
            this.IsWarning = false;
            this.MessagesWarning = new List<string>();
            this.User = user;

            this.PortfolioId = portfolioId;
            this.ContentRows = new();


            using (StreamReader sr = new(pathFile, System.Text.Encoding.Default))
            {
                string str = "";
                while ((str = sr.ReadLine()) != null)
                {
                    try
                    {

                        if (str.IndexOf('"') != -1)
                        {
                            List<string> strSubs = new();
                            string strSub = "";
                            for (int i = 0; i < str.Length; i++)
                            {
                                if (str[i] == 34)
                                {
                                    for (i++; i < str.Length; i++)
                                    {

                                        if (str[i] == 34)
                                        {
                                            strSubs.Add(strSub.Trim(','));
                                            strSub = "";
                                            i++;
                                            break;
                                        }
                                        else
                                        {
                                            strSub += str[i];
                                        }
                                    }
                                }
                                else
                                {
                                    if (str[i] == ',')
                                    {
                                        strSubs.Add(strSub);
                                        strSub = "";
                                    }
                                    else
                                    {
                                        strSub += str[i];
                                    }
                                }
                            }
                            strSubs.Add(strSub);
                            ContentRows.Add(strSubs.ToArray());
                        }
                        else
                        {
                            ContentRows.Add(str.Split(separator));
                        }
                    }
                    catch (Exception ex)
                    {
                        this.IsWarning = true;
                        this.MessagesWarning.Add(String.Format("Ошибка при обработке строки {0}{1}{2}", str, Environment.NewLine, ex.Message));
                    }


                }
            }

            _ = this.ImportTradesStocks();
            _ = this.ImportDepositsAndWithdrawals();

        }

        /// <summary>
        /// Импорт сделок
        /// </summary>
        /// <returns></returns>
        private bool ImportTradesStocks()
        {
            using (Context context = new())
            {
                // Получение портфеля, в который будут загруженны сделки
                PortfolioEntitiy portfolio = context.Portfolios.Find(PortfolioId);
                if (portfolio == null) throw new Exception(String.Format("Не найден портфель с идентификатором {0}", PortfolioId));

                // Коллекция загруженных сделок
                List<TradeEntity> tradesLoad = new();

                // Обработка распарсенных строк файла отчета
                foreach (string[] row in ContentRows.Where(x => (x[0] == "Сделки" || x[0] == "Trades") && x[1] == "Data" && (x[3] == "Акции" || x[3] == "Stocks")))
                {
                    try
                    {
                        AssetCategoryEntity assetCategory = context.AssetCategories.FirstOrDefault(x => x.Name == row[3]);
                        if (assetCategory == null)
                        {
                            assetCategory = new AssetCategoryEntity
                            {
                                Name = row[3]
                            };
                            context.AssetCategories.Add(assetCategory);
                            context.SaveChanges();
                        }
                        CurrencyEntity currency = context.Currencies.FirstOrDefault(x => x.Symbol == row[4] || x.Name == row[4]);
                        if (currency == null)
                        {
                            currency = new CurrencyEntity
                            {
                                Symbol = row[4]
                            };
                            context.Currencies.Add(currency);
                            context.SaveChanges();
                        }
                        SymbolEntity symbol = context.Symbols.FirstOrDefault(x => x.Value == row[5]);
                        if (symbol == null)
                        {
                            symbol = new SymbolEntity
                            {
                                Value = row[5]
                            };
                            context.Symbols.Add(symbol);
                            context.SaveChanges();
                        }
                        DateTime dt = DateTime.Parse(row[6]);
                        _ = int.TryParse(row[7], out int count);
                        _ = Double.TryParse(row[8], out double priceTransaction);
                        _ = Double.TryParse(row[9], out double priceClose);
                        _ = Double.TryParse(row[10], out double proceed);
                        _ = Double.TryParse(row[11], out double commission);
                        _ = Double.TryParse(row[12], out double basic);
                        _ = Double.TryParse(row[13], out double realizedPL);
                        _ = Double.TryParse(row[14], out double mtmPL);
                        string code = row[15];

                        StatusEntity status = null;
                        string[] rowStatus = ContentRows.FirstOrDefault(x => (x[0] == "Коды" || x[0] == "Codes") && x[1] == "Data" && x[2] == row[15]);
                        if (rowStatus != null)
                        {
                            status = context.Statuses.FirstOrDefault(x => x.Name.ToUpper() == rowStatus[3].ToUpper());
                            if (status == null)
                            {
                                status = new StatusEntity
                                {
                                    Name = rowStatus[3]
                                };
                                context.Statuses.Add(status);
                                context.SaveChanges();
                            }
                        }
                        TradeEntity trade = new TradeEntity
                        {
                            Portfolio = portfolio,
                            AssetCategory = assetCategory,
                            Currency = currency,
                            Symbol = symbol,
                            Count = count,
                            PriceTransaction = priceTransaction,
                            PriceClose = priceClose,
                            Proceed = proceed,
                            Commission = commission,
                            Dt = dt,
                            Basis = basic,
                            RealizedPL = realizedPL,
                            MtmPL = mtmPL,
                            Status = status
                        };
                        context.Trades.Add(trade);
                        context.SaveChanges();
                        tradesLoad.Add(trade);
                    }
                    catch (Exception ex)
                    {
                        this.IsWarning = true;
                        this.MessagesWarning.Add(String.Format("Ошибка при обработке строки {0}{1}{2}", String.Join(", ", row), Environment.NewLine, ex.Message));
                    }
                }



            }

            return true;
        }

        /// <summary>
        /// Импорт ввода и вывода средств
        /// </summary>
        /// <returns></returns>
        private bool ImportDepositsAndWithdrawals()
        {
            using (Context context = new())
            {
                // Получение портфеля, в который будут загруженны сделки
                PortfolioEntitiy portfolio = context.Portfolios.Find(PortfolioId);
                if (portfolio == null) throw new Exception(String.Format("Не найден портфель с идентификатором {0}", PortfolioId));

                // Коллекция загруженных сделок
                List<TradeEntity> tradesLoad = new List<TradeEntity>();

                // Обработка распарсенных строк файла отчета
                foreach (string[] row in ContentRows.Where(x => (x[0] == "Вводы и выводы средств" || x[0] == "Deposits & Withdrawals") && x[1] == "Data"))
                {
                    try
                    {
                        if (row.Length < 6) continue;
                        for (int i = 0; i < row.Length; i++) if (row[i] == "") continue;
                        CurrencyEntity currency = context.Currencies.FirstOrDefault(x => x.Symbol == row[2]);
                        if (currency == null)
                        {
                            IsWarning = true;
                            MessagesWarning.Add(String.Format("Не удалось выполнить импорт записи {0}.{1}Валюта {2} не поддерживается", String.Join(", ", row), Environment.NewLine, row[2]));
                            continue;
                        }
                        AssetCategoryEntity assetCategory = context.AssetCategories.FirstOrDefault(x => x.Name == currency.Name);
                        if (assetCategory == null)
                        {
                            assetCategory = new AssetCategoryEntity
                            {
                                Name = currency.Name
                            };
                            context.AssetCategories.Add(assetCategory);
                            context.SaveChanges();
                        }

                        DateTime dt = DateTime.Parse(row[3]);
                        _ = int.TryParse(row[5], out int count);


                        StatusEntity status = context.Statuses.FirstOrDefault(x => x.Name == "Вводы и выводы средств");
                        if (status == null)
                        {
                            status = new StatusEntity
                            {
                                Name = "Вводы и выводы средств"
                            };
                            context.Statuses.Add(status);
                            context.SaveChanges();

                        }

                        SymbolEntity symbol = context.Symbols.FirstOrDefault(x => x.Value == "Money");
                        if (symbol == null)
                        {
                            symbol = new SymbolEntity
                            {
                                Value = "Money",
                                Desc = "Вводы и выводы средств"
                            };
                            context.Symbols.Add(symbol);
                            context.SaveChanges();

                        }


                        TradeEntity trade = new()
                        {
                            Portfolio = portfolio,
                            AssetCategory = assetCategory,
                            Currency = currency,
                            Symbol = symbol,
                            Count = count,
                            PriceTransaction = 1,
                            PriceClose = 0,
                            Proceed = count * 1,
                            Commission = 0,
                            Dt = dt,
                            Basis = 0,
                            RealizedPL = 0,
                            MtmPL = 0,
                            Status = status



                        };
                        context.Trades.Add(trade);
                        context.SaveChanges();
                        tradesLoad.Add(trade);
                    }
                    catch (Exception ex)
                    {
                        this.IsWarning = true;
                        this.MessagesWarning.Add(String.Format("Ошибка при обработке строки {0}{1}{2}", String.Join(", ", row), Environment.NewLine, ex.Message));
                    }
                }



            }

            return true;
        }

    }


}

