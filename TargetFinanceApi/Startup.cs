using CsvHelper.Configuration;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using TargetFinanceApi.Controllers;

namespace TargetFinanceApi
{
    public class Startup
    {
        /// <summary>
        /// ������������� ����������������� ���������� �� ������ � ��������
        /// </summary>
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        /// <summary>
        /// ��������� ������������
        /// </summary>
        public IConfiguration Configuration { get; }

       
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// ��������� ����� ����������
        /// </summary>
        /// <param name="services">������ ����������</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<Context>();

            // ��������� ���������� �� ������ � ��������
            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins, builder =>
                {
                    builder.WithOrigins("http://localhost", "http://localhost:3000").AllowAnyHeader().AllowAnyMethod();
                });
            });

            // ��������� �������������� � ������� �������
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;                           // SSL ��� �������� ������ �� ������������
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,                                      // ��������� �������� ��� ��������� ������
                        ValidIssuer = AuthOptions.ISSUER,                           // �������� 
                        ValidateAudience = true,                                    // ����� �� �������������� ����������� ������
                        ValidAudience = AuthOptions.AUDIENCE,                       // ����������� ������
                        ValidateLifetime = true,                                    // ����� �� �������������� ����� �������������
                        IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),   // ���� ������������
                        ValidateIssuerSigningKey = true,                            // ��������� ����� ������������
                    };
                });

            services.AddControllers();
            services.AddControllersWithViews();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TargetFinanceApi", Version = "v1" });
            });
        }

        /// <summary>
        /// ��������� HTTP-��������.
        /// </summary>
        /// <param name="app">����������</param>
        /// <param name="env">����� �������</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TargetFinanceApi v1"));
            }
            app.UseRouting();
            app.UseCors(MyAllowSpecificOrigins);
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
